![Build Status](https://gitlab.com/vilanify/nuxt/badges/master/pipeline.svg?style=flat-square)

---

Example [Nuxt] website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---


```yml
image: node

before_script:
  - npm install

cache:
  paths:
    - node_modules/

pages:
  script:
    - npm run generate
  artifacts:
    paths:
      - public
  only:
    - master
```

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install] Nuxt
1. Generate and preview the website with hot-reloading: `npm run dev` or `nuxt`
1. Add content

----

Read more at Nuxt's [documentation].

[ci]: https://about.gitlab.com/gitlab-ci/
[Nuxt]: https://nuxtjs.org/
[install]: https://nuxtjs.org/guide/installation/
[documentation]: https://nuxtjs.org/guide
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages


